# Ruby on Rails docker build image

[Ruby Alpine docker images](https://hub.docker.com/_/ruby) are great. Little is missing to build a [Ruby on Rails](https://rubyonrails.org/) app out of the box. This is the purpose of this project: provide a Docker image based on Ruby Alpine with just the bare minimum added to it do build a simple Rails app out of the box.

## Table of content

- [Usage](#usage)
- [Credits](#credits)
- [License](#license)
- [Contributing](#contributing)

## Usage

*To be defined*

## Credits

- Author: [Kao ..98](gitlab.com/kao98)

## License

This is free and unencumbered software released into the public domain. See. [LICENSE](./LICENSE).

## Contributing

Any issues [reported here](https://gitlab.com/kao98/ruby-rails-build/-/issues) is welcome. Please see [CONTRIBUTING.md](./CONTRIBUTING.md) for more information on how to submit Merge Requests, and to find our [code of conduct](./CONTRIBUTING.md#user-content-code-of-conduct).

